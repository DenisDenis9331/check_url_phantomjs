var t0 = performance.now();
var i = 0;

var webpage = require("webpage");

var fs = require('fs');
var	file = fs.read('file.txt');
var	url = file.split(/\n/);

status_test();

function status_test()
{
    var page = webpage.create();
	page.settings.javascriptEnabled = false;
    page.settings.resourceTimeout = 10000;
    page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36';
    page.open(url[i], function(status)
	{
        if(status=="success")
		{
            console.log("success " + i + " " + url[i])

            page.render("img" + i + ".png");
            page.close();
            if(++i < url.length)
			{
				status_test();
			}
			else
			{
				var t1 = performance.now();
				console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");
				phantom.exit();
			}
        }
        else
		{
            console.log("not success " + url[i])
            page.close();
            if(++i < url.length)
			{
				status_test();
			}
            else
			{
				var t1 = performance.now();
				console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");
				phantom.exit();
			}
        }
    }			);
}